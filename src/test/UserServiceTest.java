/**
 * Created by CoolWen on 2017/4/26.
 */

import com.alibaba.fastjson.JSON;

import com.qiyou.entity.User;
import com.qiyou.service.UserService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author dell
 * @version 2017-04-26 14:58
 */
//spring进行jUnit4测试
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml", "classpath:spring-mybatis.xml"})
//测试加载我们的配置文件
public class UserServiceTest {

    //引入log4j,进行日志打印
    private static final Logger LOGGER = Logger.getLogger(UserServiceTest.class);

    @Autowired
    UserService userService;



    @Test
    public void testAddUser() {

        System.out.println(this.getClass().getResource("").getPath());


    }


    @Test
    public void testloginUser() {


        LOGGER.info("----->>>>>>" + JSON.toJSON(userService.login("18302828325","232837")));


    }
    @Test
    public void testsal() {


        LOGGER.info("----->>>>>>" + JSON.toJSON(userService.selectal("1")));


    }
    @Test
    public void testUpdateUser() {
        User user=new User();
        user.setUid("18302828325");
        user.setPhone("18302828325");
 user.setAddress("大家");
        LOGGER.info("----->>>>>>" + JSON.toJSON(userService.updateByPrimaryKeySelective(user)));


    }
}
