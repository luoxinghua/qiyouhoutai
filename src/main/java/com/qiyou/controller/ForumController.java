package com.qiyou.controller;

import com.alibaba.fastjson.JSON;
import com.qiyou.entity.Forum;
import com.qiyou.entity.Forum_commentKey;
import com.qiyou.entity.User_collection_activitiesKey;
import com.qiyou.entity.User_forumKey;
import com.qiyou.model.BaseImg;
import com.qiyou.service.UserForumService;
import com.qiyou.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by 落叶的思恋 on 2017/8/11.
 */

@Controller
@RequestMapping("/yueqi/forum")
public class ForumController {
    @Autowired
    UserForumService userForumService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public String andForum(@RequestBody Forum forum, HttpServletRequest request) throws IOException {
        String path = request.getSession().getServletContext().getRealPath("/resources/img");
        File file = new File(path);
        System.out.println(path + "文件是否存在" + file.exists());
        String img64 = forum.getImgPath();
        System.out.println("图片64" + img64);
        forum.setImgPath(new BaseImg().GenerateImage(img64, path));
        System.out.println("文件路径" + forum.getImgPath());
        if (userForumService.insert(forum) != 1) {
            return "" + 0;
        } else {
            return "" + userForumService.SelectiveId();
        }

    }

    /**
     * 更新
     **/
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateForum(@RequestBody Forum forum) throws IOException {
        User_forumKey user_forumKey = new User_forumKey();
        if (userForumService.updateByPrimaryKeySelective(forum) != 1) {
            return "" + 0;
        } else {
            user_forumKey.setFid(forum.getFid());
            user_forumKey.setUid(forum.getUid());
            return "" + userForumService.insertuf(user_forumKey);
        }

    } /**
     * 更新
     **/
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Forum updateForum(@PathVariable Integer id) throws IOException {
        Forum forum=   userForumService.selectByPrimaryKey(id);
        forum.setUser(userService.selectByPrimaryKey(forum.getUid()));
            return  forum;


    }
//    /**
//     * 更新
//     **/
//    @RequestMapping(value = "/comments", method = RequestMethod.GET)
//    @ResponseBody
//    public Forum comments( @RequestBody Forum_commentKey id) throws IOException {
//
////        forum.setUser(userService.selectByPrimaryKey(forum.getUid()));
////        return  forum;
//
//
//    }
    /**
     * 所有的
     **/
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<Forum> allForum() throws IOException {
        if (userForumService.selectall().size() > 0) {
            Forum forum = new Forum();
            List<Forum> list = userForumService.selectall();
            for (int i = 0; i < list.size(); i++) {
                String uid = userForumService.Uid(list.get(i).getFid());
                list.get(i).setUser(userService.selectByPrimaryKey(uid))  ;
                System.out.println(JSON.toJSONString(list.get(i)));
            }
            return list;
        } else {
            return null;
        }

    }

    /**
     * 详情
     **/
    @RequestMapping(value = "/information", method = RequestMethod.POST)
    @ResponseBody
    public Forum informationForum(@RequestBody User_forumKey user_forumKey) throws IOException {

        Forum Forum = new Forum();
        Forum = userForumService.selectByPrimaryKey(user_forumKey.getFid());

        Forum.setUser(userService.selectByPrimaryKey(userForumService.Uid(user_forumKey.getFid())));

        return Forum;
    }

    /**
     *
     * */
    @RequestMapping(value = "/{searcher}/titles", method = RequestMethod.GET)
    @ResponseBody
    public List<Forum> searcherForum(@PathVariable String searcher) throws IOException {
        System.out.println("详情" + searcher);
        List<Forum> list = userForumService.selectsearch(searcher);
        for(int i=0;i<list.size();i++){
            list.get(i).setUser(userService.selectByPrimaryKey(userForumService.Uid(list.get(i).getFid())));

        }
        return list;
    }
}
