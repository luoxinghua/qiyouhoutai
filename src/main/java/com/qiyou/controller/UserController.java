package com.qiyou.controller;

import com.alibaba.fastjson.JSON;
import com.qiyou.dao.User_join_activitiesMapper;
import com.qiyou.entity.*;
import com.qiyou.model.BaseImg;
import com.qiyou.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 落叶的思恋 on 2017/7/25.
 */
@Controller
@RequestMapping("/yueqi")
public class UserController {
    private static final Logger LOGGER = Logger.getLogger(UserController.class);
    @Autowired
    ActivityService activityService;
    @Autowired
    UserService userService;

    @Autowired
    UserJionactivity userJionactivity;

    @Autowired
    Userconllectionactivity userconllectionactivity;
    @Autowired
    UserForumService userForumService;
    /**
     * 用户登录
     */
    @ResponseBody
    @RequestMapping(value = {"/users/login"}, method = RequestMethod.POST)
    public User UserLoginPost(Model model, @RequestBody User user) {
//        System.out.println("开始登陆");
        user=  userService.login(user.getPhone(), user.getPassword());
//        System.out.println("登录"+JSON.toJSONString(user));

//        System.out.println("我收藏的团"+userService.selectalc(user.getUid()));
        if(user!=null){
            if(userService.selectal(user.getUid())!=null){
                user.setAl(""+userService.selectal(user.getUid()));
            }
            if(userService.selectal(user.getUid())!=null){
                user.setAlc(""+userService.selectalc(user.getUid()));
            } if(userService.selectal(user.getUid())!=null){
                user.setFl(""+userService.selectml(user.getUid()));
            }
        }


        return user;
    }

    /**
     * 用户注册
     */
    @ResponseBody
    @RequestMapping(value = {"/register"}, method = RequestMethod.POST)
    public Integer UserRegisterPost(Model model, @RequestBody User user) {
        int num = 0;
        user.setPhone(user.getUid());
        System.out.println("注册的用户" + JSON.toJSONString(user));
        num = userService.insert(user);

        return num;
    }

    /**
     * 用户修改信息
     */
    @ResponseBody
    @RequestMapping(value = {"/users/setting"}, method = RequestMethod.POST)
    public User UserSetingPost(Model model, @RequestBody User user) {
        System.out.println("用户"+JSON.toJSONString(user));
        if (userService.updateByPrimaryKeySelective(user)!= 1) {
            return null;
        } else {
            return userService.selectByPrimaryKey(user.getUid());
        }
    }

    /**
     * 头像上传
     */
    @RequestMapping(value = "/users/upload", method = RequestMethod.POST)
    @ResponseBody
    public User insertCate(@RequestBody User user, HttpServletRequest request) throws IOException {
        String path = request.getSession().getServletContext().getRealPath("/resources/img");
        File file = new File(path);
        System.out.println(path+"文件是否存在"+file.exists());
        String img64 = user.getImagePath();
        System.out.println("图片64" + img64);
        user.setImagePath(new BaseImg().GenerateImage(img64, path));
        System.out.println("文件路径" + user.getImagePath());
         if(userService.updateByPrimaryKeySelective(user)!=1){
             return null;
         }else {
             return userService.selectByPrimaryKey(user.getUid());
         }
    }
    @RequestMapping(value = "/users/join", method = RequestMethod.POST   )
    @ResponseBody
    public int joinActivity(@RequestBody User_join_activitiesKey userjoin) throws IOException {
    return userJionactivity.insert(userjoin);
    }


    @RequestMapping(value = "/users/collection", method = RequestMethod.POST)
    @ResponseBody
    public int collectionActivity(@RequestBody User_collection_activitiesKey user_join_activitiesKey) throws IOException {
      return  userconllectionactivity.insert(user_join_activitiesKey);
    }

    @RequestMapping(value = "/users/cal/{uid}", method = RequestMethod.GET)
    @ResponseBody
    public List<Activity> mycollectionActivity(@PathVariable String uid) throws IOException {
        List<Activity> list=new ArrayList<Activity>() ;

        List<Integer> list1= userconllectionactivity.selectcal(uid);

        if(list1.size()>0){
            for (int i=0;i<list1.size();i++){
                System.out.println(uid+"收藏的活动有哪些"+JSON.toJSONString(list1));
                Activity activity=new Activity();
                activity=activityService.selectByPrimaryKey(list1.get(i));
                activity.setJionnum(userJionactivity.selectjul(list1.get(i)));
                list.add(activity);
            }
        }
        return  list;
    }

    @RequestMapping(value = "/users/jal/{uid}", method = RequestMethod.GET)
    @ResponseBody
    public List<Activity> myjioncollectionActivity(@PathVariable String uid) throws IOException {
        List<Activity> list=new ArrayList<Activity>() ;
        List<Integer> list1=userJionactivity.selectjal(uid);
        System.out.println(uid+"参加的活动有哪些"+JSON.toJSONString(list1));
        if(list1.size()>0){
            for (int i=0;i<list1.size();i++){
                System.out.println(uid+"参加的活动有哪些"+list1.get(i));
                Activity activity=new Activity();
                activity=activityService.selectByPrimaryKey(list1.get(i));
                activity.setJionnum(userJionactivity.selectjul(list1.get(i)));
                list.add(activity);
            }
        }

        return list ;
    }



    @RequestMapping(value = "/users/fl/{uid}", method = RequestMethod.GET)
    @ResponseBody
    public List<Forum> myFrom(@PathVariable String uid) throws IOException {
        List<Forum> list= new ArrayList<Forum>();
        List<Integer> list1=userForumService.selectmfl(uid);
        if(list1.size()>0){
            for (int i=0;i<list1.size();i++){
                list.add(userForumService.selectByPrimaryKey(list1.get(i)));
            }
        }
        return list ;
    }
    @RequestMapping(value = "/users/userforum/save", method = RequestMethod.POST)
    @ResponseBody
    public String myFrom( @RequestBody Forum forum) throws IOException {
 int ii=userForumService.insert(forum);
        return ""+ii;
    }
}
