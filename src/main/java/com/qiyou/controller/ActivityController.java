package com.qiyou.controller;

import com.alibaba.fastjson.JSON;
import com.qiyou.entity.Activity;
import com.qiyou.entity.Forum;
import com.qiyou.entity.User;
import com.qiyou.entity.User_collection_activitiesKey;
import com.qiyou.model.BaseImg;
import com.qiyou.service.ActivityService;
import com.qiyou.service.UserJionactivity;
import com.qiyou.service.UserService;
import com.qiyou.service.Userconllectionactivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by 落叶的思恋 on 2017/8/7.
 */
@Controller
@RequestMapping("/yueqi/activities")
public class ActivityController {
    @Autowired
    ActivityService activityService;
    @Autowired
    UserJionactivity userJionactivity;
    @Autowired
    Userconllectionactivity userconllectionactivity;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public String andActivity(@RequestBody Activity activity, HttpServletRequest request) throws IOException {
        String path = request.getSession().getServletContext().getRealPath("/resources/img");
        File file = new File(path);
        System.out.println(path + "文件是否存在" + file.exists());
        String img64 = activity.getImagePath();
        System.out.println("图片64" + img64);
        activity.setImagePath(new BaseImg().GenerateImage(img64, path));
        System.out.println("文件路径" + activity.getImagePath());
        if (activityService.insertSelective(activity) != 1) {
            return "" + 0;
        } else {
            return "" + activityService.SelectiveId();
        }

    }

    /**
     * 更新
     **/
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateActivity(@RequestBody Activity activity) throws IOException {

        if (activityService.updateByPrimaryKeySelective(activity) != 1) {
            return "" + 1;
        } else {
            return "" + 0;
        }

    }
    /**
     * 更新
     **/
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public String save(@RequestBody Activity activity) throws IOException {

        if (activityService.insertSelective(activity) != 1) {
            return "" + 1;
        } else {
            return "" + 0;
        }

    }
    /**
     * 所有的
     **/
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<Activity> allActivity() throws IOException {
        List<Activity> list=activityService.selectall();
        if(list.size()>0){
            for(int i=0;i<list.size();i++){

                list.get(i).setJionnum(userJionactivity.selectjul(list.get(i).getAid()));

            }
        }
        return list;
    }
    /**
     * 更新
     **/
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Activity updateForum(@PathVariable Integer id) throws IOException {
        Activity activity=   activityService.selectByPrimaryKey(id);
        return  activity;


    }
    /**
     * 详情
     **/
    @RequestMapping(value = "/information", method = RequestMethod.POST)
    @ResponseBody
    public Activity informationActivity(@RequestBody User_collection_activitiesKey user_activitiesKey) throws IOException {
        System.out.println("详情");
        Activity activity = new Activity();
        activity = activityService.selectByPrimaryKey(user_activitiesKey.getAid());
        if (userconllectionactivity.selectcollectionuser(user_activitiesKey.getAid(), user_activitiesKey.getUid()) != null) {
            activity.setCollecation(1);
        } else {
            activity.setCollecation(null);
        }
        if (userJionactivity.selectjion(user_activitiesKey.getAid(), user_activitiesKey.getUid()) != null) {
            activity.setJion(1);
        } else {
            activity.setJion(null);
        }
        System.out.println(JSON.toJSONString(activity));

        return activity;
    }

    /**
     *
     * */
    @RequestMapping(value = "/{searcher}/titles", method = RequestMethod.GET)
    @ResponseBody
    public List<Activity> searcherActivity(@PathVariable String searcher) throws IOException {
        System.out.println("详情" + searcher);
        return activityService.selectsearch(searcher);
    }
}
