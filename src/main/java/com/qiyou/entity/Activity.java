package com.qiyou.entity;

import java.util.Date;
import java.util.List;

public class Activity {
    private Integer aid;

    private String activityRoute;

    private String contact;

    private String endTime;

    private String headLine;

    private String imagePath;

    private String startTime;

    private Date time;

    private Integer userCount;

    private String uid;

    private String detials;

    public Integer getJionnum() {
        return jionnum;
    }

    public void setJionnum(Integer jionnum) {
        this.jionnum = jionnum;
    }

    private  Integer jionnum;
    public Integer getCollecation() {
        return collecation;
    }

    public void setCollecation(Integer collecation) {
        this.collecation = collecation;
    }

    public Integer getJion() {
        return jion;
    }

    public void setJion(Integer jion) {
        this.jion = jion;
    }

    private Integer collecation;
    private Integer jion;

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public String getActivityRoute() {
        return activityRoute;
    }

    public void setActivityRoute(String activityRoute) {
        this.activityRoute = activityRoute == null ? null : activityRoute.trim();
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact == null ? null : contact.trim();
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime == null ? null : endTime.trim();
    }

    public String getHeadLine() {
        return headLine;
    }

    public void setHeadLine(String headLine) {
        this.headLine = headLine == null ? null : headLine.trim();
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath == null ? null : imagePath.trim();
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime == null ? null : startTime.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid == null ? null : uid.trim();
    }

    public String getDetials() {
        return detials;
    }

    public void setDetials(String detials) {
        this.detials = detials == null ? null : detials.trim();
    }
}