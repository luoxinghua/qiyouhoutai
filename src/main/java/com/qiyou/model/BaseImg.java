package com.qiyou.model;

import org.codehaus.plexus.util.Base64;
import sun.misc.BASE64Decoder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

/**
 * Created by 落叶的思恋 on 2017/6/10.
 */
public class BaseImg {
    public String GenerateImage(String imgStr,String realpath) throws IOException {
        try {
            BASE64Decoder decoder = new BASE64Decoder();
        byte[] b = decoder.decodeBuffer(imgStr);
        for (int i = 0; i < b.length; ++i) {
            if (b[i] < 0) {
                b[i] += 256;
            }
        }

        Date date=new Date();
        long time=date.getTime();
            System.out.println("当前时间"+time);
        String imgPath=realpath+"\\"+time+".jpg";
      OutputStream out = new FileOutputStream(imgPath);
        out.write(b);
        out.flush();
        out.close();
            System.out.println("1111111111111"+imgPath);
        return time+".jpg";
        }catch (Exception e){
            e.getMessage();
            return "";
        }
    }
}
