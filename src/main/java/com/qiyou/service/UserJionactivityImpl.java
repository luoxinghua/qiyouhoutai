package com.qiyou.service;

import com.qiyou.dao.User_join_activitiesMapper;
import com.qiyou.entity.User_join_activitiesKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 落叶的思恋 on 2017/8/10.
 */
@Service("UserJionactivity")
public class UserJionactivityImpl implements UserJionactivity {
    @Autowired
    User_join_activitiesMapper user_join_activitiesMapper;

    @Override
    public int insert(User_join_activitiesKey record) {

        return user_join_activitiesMapper.insert(record);
    }

    @Override
    public List<Integer> selectjal(String uid) {
        return  user_join_activitiesMapper.selectjal(uid);
    }

    @Override
    public User_join_activitiesKey selectjion(Integer aid, String uid) {
        return user_join_activitiesMapper.selectjionuser(aid,uid);
    }

    @Override
    public Integer selectjul(Integer aid) {
        return user_join_activitiesMapper.selectjul(aid);
    }


}
