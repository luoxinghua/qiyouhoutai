package com.qiyou.service;

import com.qiyou.entity.Activity;
import com.qiyou.entity.Forum;
import com.qiyou.entity.Forum_commentKey;
import com.qiyou.entity.User_forumKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 落叶的思恋 on 2017/8/11.
 */
public interface UserForumService {
    List<Integer> selectmfl(@Param("uid") String uid);

    int insert(Forum record);

    Forum selectByPrimaryKey(Integer fid);

    int updateByPrimaryKeySelective(Forum record);

    List<Forum> selectall();

    String  Uid(@Param("fid") Integer fid);
    List<Forum> selectsearch(@Param("search") String search);

    int SelectiveId();
    int comment(Forum_commentKey record);
    int insertuf(User_forumKey record);
}
