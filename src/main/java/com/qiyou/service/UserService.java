package com.qiyou.service;/**
 * Created by CoolWen on 2017/5/10.
 */


import com.qiyou.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CoolWen
 * @version 2017-05-10 14:34
 */
public interface UserService {
    User login(String Username, String password);

    int insert(User record);

    User selectByPrimaryKey(String uid);

    int updateByPrimaryKeySelective(User record);
    Integer selectal(String uid);

    Integer selectalc(String uid);

    Integer selectml(String uid);

}
