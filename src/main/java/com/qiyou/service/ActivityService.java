package com.qiyou.service;

import com.qiyou.entity.Activity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 落叶的思恋 on 2017/8/7.
 */
public interface ActivityService {
    int updateByPrimaryKeySelective(Activity record);
     int insertSelective(Activity record);
    int SelectiveId();
    List<Activity> selectall();
    List<Activity> selectsearch(String search);
    Activity selectByPrimaryKey(Integer aid);
}
