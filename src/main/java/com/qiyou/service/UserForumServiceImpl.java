package com.qiyou.service;

import com.qiyou.dao.ForumMapper;
import com.qiyou.dao.Forum_commentMapper;
import com.qiyou.dao.User_forumMapper;
import com.qiyou.entity.Activity;
import com.qiyou.entity.Forum;
import com.qiyou.entity.Forum_commentKey;
import com.qiyou.entity.User_forumKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 落叶的思恋 on 2017/8/11.
 */
@Service("UserForumService")
public class UserForumServiceImpl implements UserForumService {

    @Autowired
    User_forumMapper user_forumMapper;
    @Autowired
    ForumMapper forumMapper;
    @Autowired
    Forum_commentMapper forum_commentMapper;
    @Override
    public List<Integer> selectmfl(String uid) {
        return user_forumMapper.selectmfl(uid);
    }

    @Override
    public int insert(Forum record) {
        return forumMapper.insertSelective(record);
    }


    @Override
    public Forum selectByPrimaryKey(Integer fid) {
        return forumMapper.selectByPrimaryKey(fid);
    }

    @Override
    public int updateByPrimaryKeySelective(Forum record) {
        return forumMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public List<Forum> selectall() {
        return forumMapper.selectall();
    }

    @Override
    public String Uid(Integer fid) {
        return user_forumMapper.Uid(fid);
    }

    @Override
    public List<Forum> selectsearch(String search) {
        return forumMapper.selectsearch(search);
    }

    @Override
    public int SelectiveId() {
        return forumMapper.SelectiveId();
    }

    @Override
    public int comment(Forum_commentKey record) {
        return 0;
    }

    @Override
    public int insertuf(User_forumKey record) {
        return user_forumMapper.insert(record);
    }


}
