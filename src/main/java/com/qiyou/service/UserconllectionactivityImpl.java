package com.qiyou.service;

import com.qiyou.dao.ActivityMapper;
import com.qiyou.dao.User_collection_activitiesMapper;
import com.qiyou.entity.User_collection_activitiesKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 落叶的思恋 on 2017/8/10.
 */
@Service("Userconllectionactivity")
public class UserconllectionactivityImpl implements Userconllectionactivity {
    @Autowired
    User_collection_activitiesMapper user_collection_activitiesMapper;
    @Override
    public int insert(User_collection_activitiesKey record) {
        return user_collection_activitiesMapper.insert(record);
    }

    @Override
    public User_collection_activitiesKey selectcollectionuser(Integer aid, String uid) {
        return user_collection_activitiesMapper.selectcollectionuser(aid,uid);
    }

    @Override
    public List<Integer> selectcal(String uid) {
        return user_collection_activitiesMapper.selectcal(uid);
    }


}
