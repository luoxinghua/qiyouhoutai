package com.qiyou.service;/**
 * Created by CoolWen on 2017/5/10.
 */

import com.qiyou.dao.UserMapper;
import com.qiyou.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 * @author dell
 * @version 2017-05-10 14:36
 */
@Service("UserService")
public class UserServiceImpl implements UserService {

   @Autowired
    UserMapper userMapper;

/**
 * 用户登录
 * */
    @Override
    public User login(String Username, String password) {
        return userMapper.login(Username,password);
    }

    /**
     * 用户注册
     * */

    @Override
    public int insert(User record) {
        return userMapper.insert(record);
    }

    @Override
    public User selectByPrimaryKey(String uid) {
        return userMapper.selectByPrimaryKey(uid);
    }

    @Override
    public int updateByPrimaryKeySelective(User record) {
        return userMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public Integer selectal(String uid) {
        return userMapper.selectal(uid);
    }

    @Override
    public Integer selectalc(String uid) {
        return userMapper.selectalc(uid);
    }

    @Override
    public Integer selectml(String uid) {
        return userMapper.selectml(uid);
    }
}
