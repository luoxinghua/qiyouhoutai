package com.qiyou.service;

import com.qiyou.dao.ActivityMapper;
import com.qiyou.entity.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 落叶的思恋 on 2017/8/7.
 */
@Service("ActivityService")
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    ActivityMapper activityMapper;
    @Override
    public int updateByPrimaryKeySelective(Activity record) {
        return activityMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int insertSelective(Activity record) {
        return activityMapper.insertSelective(record);
    }

    @Override
    public int SelectiveId() {
        return activityMapper.SelectiveId();
    }

    @Override
    public List<Activity> selectall() {
        return activityMapper.selectall();
    }

    @Override
    public List<Activity> selectsearch(String search) {
        return activityMapper.selectsearch(search);
    }

    @Override
    public Activity selectByPrimaryKey(Integer aid) {
        return activityMapper.selectByPrimaryKey(aid);
    }
}
