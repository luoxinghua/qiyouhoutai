package com.qiyou.dao;

import com.qiyou.entity.Activity;
import com.qiyou.entity.Forum_commentKey;

import java.util.List;

public interface Forum_commentMapper {
    int deleteByPrimaryKey(Forum_commentKey key);

    int insert(Forum_commentKey record);

    int insertSelective(Forum_commentKey record);


}