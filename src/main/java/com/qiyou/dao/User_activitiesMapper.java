package com.qiyou.dao;

import com.qiyou.entity.User_activitiesKey;

public interface User_activitiesMapper {
    int deleteByPrimaryKey(User_activitiesKey key);

    int insert(User_activitiesKey record);

    int insertSelective(User_activitiesKey record);
}