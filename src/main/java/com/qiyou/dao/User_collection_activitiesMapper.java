package com.qiyou.dao;

import com.qiyou.entity.User_collection_activitiesKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface User_collection_activitiesMapper {
    int deleteByPrimaryKey(User_collection_activitiesKey key);

    List<Integer> selectcal(@Param("uid") String uid);

    int insert(User_collection_activitiesKey record);

    User_collection_activitiesKey selectcollectionuser(@Param("aid") Integer aid, @Param("uid") String uid);

    int insertSelective(User_collection_activitiesKey record);
}