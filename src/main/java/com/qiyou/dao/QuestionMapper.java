package com.qiyou.dao;

import com.qiyou.entity.Question;

public interface QuestionMapper {
    int deleteByPrimaryKey(Integer hid);

    int insert(Question record);

    int insertSelective(Question record);

    Question selectByPrimaryKey(Integer hid);

    int updateByPrimaryKeySelective(Question record);

    int updateByPrimaryKey(Question record);
}