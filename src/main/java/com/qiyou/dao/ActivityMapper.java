package com.qiyou.dao;

import com.qiyou.entity.Activity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityMapper {
    int deleteByPrimaryKey(Integer aid);

    int insert(Activity record);

    int insertSelective(Activity record);

    Activity selectByPrimaryKey(Integer aid);
   List<Activity>  selectall();
    int SelectiveId();

    List<Activity> selectsearch(@Param("search") String search);
    int updateByPrimaryKeyWithBLOBs(Activity record);


    int updateByPrimaryKey(Activity record);
   int updateByPrimaryKeySelective(Activity record);
}