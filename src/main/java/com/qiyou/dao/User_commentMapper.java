package com.qiyou.dao;

import com.qiyou.entity.User_commentKey;

public interface User_commentMapper {
    int deleteByPrimaryKey(User_commentKey key);

    int insert(User_commentKey record);

    int insertSelective(User_commentKey record);
}