package com.qiyou.dao;

import com.qiyou.entity.comment;

public interface commentMapper {
    int deleteByPrimaryKey(Integer cid);

    int insert(comment record);

    int insertSelective(comment record);

    comment selectByPrimaryKey(Integer cid);

    int updateByPrimaryKeySelective(comment record);

    int updateByPrimaryKey(comment record);
}