package com.qiyou.dao;

import com.qiyou.entity.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    int deleteByPrimaryKey(String uid);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String uid);

    int selectal(String uid);

    int selectalc(String uid);

    int selectml(String uid);

    User login(@Param("username") String Username, @Param("password") String password);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}