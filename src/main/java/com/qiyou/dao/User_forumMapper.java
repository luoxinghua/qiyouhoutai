package com.qiyou.dao;

import com.qiyou.entity.User_forumKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface User_forumMapper {
    int deleteByPrimaryKey(User_forumKey key);

    List<Integer> selectmfl(@Param("uid") String uid);

    String  Uid(@Param("fid") Integer fid);
    int insert(User_forumKey record);

    int insertSelective(User_forumKey record);
}