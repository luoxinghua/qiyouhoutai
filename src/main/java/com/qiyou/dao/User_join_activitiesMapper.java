package com.qiyou.dao;

import com.qiyou.entity.User_join_activitiesKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface User_join_activitiesMapper {
    int deleteByPrimaryKey(User_join_activitiesKey key);

    List<Integer> selectjal(@Param("uid") String uid);

    Integer selectjul(@Param("aid") Integer aid);

    int insert(User_join_activitiesKey record);

    User_join_activitiesKey selectjionuser(@Param("aid") Integer aid, @Param("uid") String uid);

    int insertSelective(User_join_activitiesKey record);
}