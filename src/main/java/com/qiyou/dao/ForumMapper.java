package com.qiyou.dao;

import com.qiyou.entity.Activity;
import com.qiyou.entity.Forum;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ForumMapper {
    int deleteByPrimaryKey(Integer fid);

    int insert(Forum record);

    int insertSelective(Forum record);

    Forum selectByPrimaryKey(Integer fid);

    List<Forum> selectall();

    int updateByPrimaryKeySelective(Forum record);

    int updateByPrimaryKeyWithBLOBs(Forum record);

    int updateByPrimaryKey(Forum record);

    List<Forum> selectsearch(@Param("search") String search);

    int SelectiveId();
}